const {proxy} = require('../config')
const {resUserStore, userAliasStore, proxyStore,lastUserStore} = require('../store')

const proxyAction = async (req) =>{
  if(!proxy.isProxy) return 
  if(req.self()) return
  if(req.from().type() !== 1) return

  const type = req.type()
  const fromId = req.from().id
  const userNum = resUserStore.get(fromId)
  let msg = 
      `${req.from().name()}-(${userNum}): \n`
  
    for(let user of proxyStore.values()){
    // 自己发送的信息，进行过滤
    if(fromId === user.id) return
    switch(type){
      case 7:
      msg += `   👉 : ${req.text()}`
      user.say(msg)
      break;
      default:
      user.say(msg)
      req.forward(user)
      break;
    }
  }
}

const addUser = async (number, contact) =>{
  resUserStore.set(contact.id, number)
  userAliasStore.set(number, contact)
}

/**
 * 转发信息
 * @param {*} id 用户ID
 * @param {*} msg 消息Object
 */
const resAction = async (id, msg, type) =>{
  if(!id || !msg) return
  const contact = userAliasStore.get(+id)
  if(contact){
    if(type === 'forward'){
      await msg.forward(contact)
    }else{
      await contact.say(msg)
    }
  }
}


/**
 * 返回好友列表
 */
const friendList = ()=>{
  let nameList = [];
      for(let [key, value] of userAliasStore){
        nameList.push(`${key}: ${value.name()}`)
      }
  return nameList.join(`\n`)
}

const bindList = ()=>{
  let nameList = [];
  for(let id of lastUserStore.users){
    nameList.push(`${id}:${userAliasStore.get(+id).name()}`)
  }
  return nameList.join(`\n`)
}

module.exports = {
  proxyAction,
  addUser,
  resAction,
  friendList,
  bindList
}