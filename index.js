const {Wechaty} = require('wechaty')
const qt = require('qrcode-terminal')
const {proxyAction,addUser, resAction,friendList,bindList} = require('./utils/proxy')
const {proxy} = require('./config')
const {proxyStore, userAliasStore,lastUserStore} = require('./store')

const bot = new Wechaty({name:"bot"})

bot
.on('logout', onLogout)
.on('login',  onLogin)
.on('scan',   onScan)
.on('error',  onError)
.on('message', onMessage)

bot.start()
.catch(async e => {
  console.error('Bot start() fail:', e)
  await bot.stop()
  process.exit(-1)
})

function onScan (qrcode, status) {
  qt.generate(qrcode, { small: true })
}

async function onLogin (user) {
  console.log(`${user.name()} login`)
  bot.say('Wechaty login').catch(console.error)
  await main()
}

function onLogout (user) {
  console.log(`${user.name()} logouted`)
}

function onError (e) {
  console.error('Bot error:', e)
}


async function onMessage (msg) {
  if(msg.room() || msg.self()) return
  const text = msg.payload.text
  const id = msg.payload.fromId
  const contact = await bot.Contact.load(id)
  const resReg = /^(\d+)\s+([\u4e00-\u9fa5\w\W]*)$/ig
  const bindReg = /^(bind)\s+(\d+\s?)+/ig
  const bindUserReg = /\d+/ig

  switch(text){
    case 'help':
    msg.say(helpMessage())
      break;
    case 'proxy':
      proxyStore.set(id, contact)
      break;
    case 'unproxy':
    if(proxyStore.has(id)) proxyStore.delete(id)
    break;
    case 'list':
    if(proxyStore.has(id)) msg.say(friendList())
      break;
    case 'bindlist':
    if(proxyStore.has(id)) msg.say(bindList())
      break;
    case text.match(bindReg) && text.match(bindReg)[0]:
      if(proxyStore.has(id)) {
        const idArr = text.match(bindUserReg)
        lastUserStore.users = new Set(idArr)
      }
      break;
    case 'unbind':
      if(proxyStore.has(id)) lastUserStore.users = new Set()
      break;
    case text.match(resReg) && text.match(resReg)[0]:
      if(proxyStore.has(id)){
        resReg.lastIndex = 0
        let regMsg = resReg.exec(text)
        resAction(regMsg && regMsg[1], regMsg && regMsg[2])
      }
      break;
    default:
      for(let user of lastUserStore.users){
        if(proxyStore.has(id)) {
          if(!proxyStore.has(userAliasStore.get(+user).id)) resAction(user, msg, 'forward')
        }else{
          proxyAction(msg)
        }
      }
    break;
  }
}

const main = async ()=> {
  const contactList = await bot.Contact.findAll()
  let a = 0
  for(let i of contactList){
    if(i.type() === 1 && i.friend()){
      addUser(++a, i)
    }
  }
  const friends = friendList()
  //console.log(friends)
}

const helpMessage = ()=>{
  return `
  proxy: 代理
  unproxy: 解除代理
  list: 查看好友列表
  bindlist: 查看绑定发送人的好友
  bind: 绑定好友发送信息
  unbind:取消所有绑定人
  help: 帮助
  `
}