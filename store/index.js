// key: user_id, value data
const resUserStore = new Map()
const userAliasStore = new Map()
const proxyStore = new Map()
const lastUserStore = {users:new Set()}
module.exports = {
  resUserStore,
  userAliasStore,
  proxyStore,
  lastUserStore
}